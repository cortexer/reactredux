#!/bin/env node
//  OpenShift sample Node application
var restify = require('restify');
var mongojs = require("mongojs");

var ip_addr = process.env.OPENSHIFT_NODEJS_IP   || '127.0.0.1';
var port    = process.env.OPENSHIFT_NODEJS_PORT || '8080';

var db_name = process.env.OPENSHIFT_APP_NAME || "localstudents";

var connection_string = '127.0.0.1:27017/' + db_name;
// if OPENSHIFT env variables are present, use the available connection info:
if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD){
  connection_string = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
  process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
  process.env.OPENSHIFT_APP_NAME;
}

var db = mongojs(connection_string, [db_name]);
var students = db.collection("students");
var items = db.collection("items");


var server = restify.createServer({
    name : "localstudents"
});

server.pre(restify.pre.userAgentConnection());
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());

function findAllstudents(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    students.find().limit(20).sort({postedOn : -1} , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(200 , success);
            return next();
        }else{
            return next(err);
        }
        
    });
    
}

function findStudent(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    students.findOne({_id:mongojs.ObjectId(req.params.studentId)} , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(200 , success);
            return next();
        }
        return next(err);
    })
}

function updateStudent(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    students.findOne({_id:mongojs.ObjectId(req.params.studentId)} , function(err , student){
    if (err)
      res.send(err);

    // Update the existing name
    student.nimi = req.params.nimi;
	student.email = req.params.email;
	student.opiskelijanumero = req.params.opiskelijanumero;
	student.opintopisteet = req.params.opintopisteet;	

    // Save the student and check for errors
    students.save(student , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(201 , student);
            return next();
        }else{
            return next(err);
        }
    });
  });
};

function postNewStudent(req , res , next){
    var student = {};
    student.nimi = req.params.nimi;
    student.email = req.params.email;
    student.opiskelijanumero = req.params.opiskelijanumero;
    student.opintopisteet = req.params.opintopisteet;

    res.setHeader('Access-Control-Allow-Origin','*');
    
    students.save(student , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(201 , student);
            return next();
        }else{
            return next(err);
        }
    });
}

function deleteStudent(req , res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    students.remove({_id:mongojs.ObjectId(req.params.studentId)} , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(204);
            return next();      
        } else{
            return next(err);
        }
    })
    
}

function findAllItems(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    items.find().limit(20).sort({postedOn : -1} , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(200 , success);
            return next();
        }else{
            return next(err);
        }
        
    });
    
}

function findItem(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    items.findOne({_id:mongojs.ObjectId(req.params.itemId)} , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(200 , success);
            return next();
        }
        return next(err);
    })
}

function updateItem(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    items.findOne({_id:mongojs.ObjectId(req.params._id)} , function(err , item){
    if (err)
      res.send(err);

    // Update the existing name
    item.author = req.params.author;
	item.text = req.params.text;	

    // Save the student and check for errors
    items.save(item , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(201 , item);
            return next();
        }else{
            return next(err);
        }
    });
  });
};

function postNewItem(req , res , next){
    var item = {};
    item.author = req.params.author;
	item.text = req.params.text;

    res.setHeader('Access-Control-Allow-Origin','*');
    
    items.save(item , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(201 , item);
            return next();
        }else{
            return next(err);
        }
    });
}

function deleteItem(req , res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    items.remove({_id:mongojs.ObjectId(req.params._id)} , function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(204);
            return next();      
        } else{
            return next(err);
        }
    })
    
}


var PATH = '/students';

server.get({path : PATH , version : '0.0.1'} , findAllstudents);
server.get({path : PATH +'/:studentId' , version : '0.0.1'} , findStudent);
server.post({path : PATH , version: '0.0.1'} ,postNewStudent);
server.del({path : PATH +'/:studentId' , version: '0.0.1'} ,deleteStudent);
server.put({path : PATH +'/:studentId' , version: '0.0.1'} ,updateStudent);

var PATH2 = '/items';

server.get({path : PATH2 , version : '0.0.1'} , findAllItems);
server.get({path : PATH2 +'/:studentId' , version : '0.0.1'} , findItem);
server.post({path : PATH2 , version: '0.0.1'} ,postNewItem);
server.del({path : PATH2 +'/:studentId' , version: '0.0.1'} ,deleteItem);
server.put({path : PATH2 +'/:studentId' , version: '0.0.1'} ,updateItem);

var defaultDoc = "reactredux.html";

server.get('/', restify.serveStatic({
  directory: './',
  default: 'reactredux.html'
}));

server.listen(port ,ip_addr, function(){
    console.log('%s listening at %s ', server.name , server.url);
})

